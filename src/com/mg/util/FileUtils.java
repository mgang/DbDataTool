package com.mg.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * 文件工具类
 * @author meigang 2015-12-21
 *
 */
public class FileUtils {
	/**
	 * 获取文本文件所有内容
	 * @param fileurl 文件url
	 * @return 返回文件中所有内容
	 * @throws Exception 
	 */
	public static String loadFileContent(String fileurl) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(fileurl));
		String all = "";
		String line = "";
		while(null != (line = br.readLine())){
			//去前后空格
			line = StringUtils.reverse(StringUtils.reverse(line.trim()).trim());
			all += line;
		}
		br.close();
		return all;
	}
}
