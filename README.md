#DbDataTool
    最近在做一个项目时，遇到一个问题。原先SqlServer数据库，通过某程序转换到Mysql中的时候，有些表数据是跟着过来的。但是有写只是表结构过来了，数据去没有。
    
    于是，为了不手动去导数据，这就写了个小程序来完成这样一件事。
    
##1.项目架构
    JFinal + JFinal-ext 
##2.版本介绍
    目前生存了2个版本，v1.0和v2.0。
###v1.0
    直接在配置文件中配置好数据源和目标源，以及要转移数据的表名。
    配置如下：
    
``` txt
#from db config
from.dataSource.url=jdbc\:mysql\://127.0.0.1\:3306/from?characterEncoding\=utf8&zeroDateTimeBehavior\=convertToNull
from.dataSource.userName=root
from.dataSource.password=root
from.dataSource.driverClass=com.mysql.jdbc.Driver


#to db config
to.dataSource.url=jdbc\:mysql\://127.0.0.1\:3306/to?characterEncoding\=utf8&zeroDateTimeBehavior\=convertToNull
to.dataSource.userName=root
to.dataSource.password=root
to.dataSource.driverClass=com.mysql.jdbc.Driver

#table name
table.name=template_config
```
    直接运行项目，点击主界面上的开始按钮，即可完成数据转移。
    
###v2.0
    将配置从配置文件转移到界面中，让用户自己手动输入。这样做更加灵活，人性化。
    界面如下：
![](./img/db_tool_main.png)

##3.项目转型获新生
	考虑到以后还有很多关于数据库数据处理的小程序，这里我就给这个项目前端加上了一个框架布局，并且加入了一些前端常用的ui，如bootstarp,miniui,layer,font awesome等。
	现阶段加入的是代码生成小工具；已完成的功能列表：
	1.根据数据库表生成JavaBean对象的功能；
	2.根据数据库表生成mybatis xml文件中的resultMap代码的功能；
	3.根据数据库表生成mybatis xml文件中常用的cud的标准代码的功能；
	4....待续
##4.gen code截图
	do_javabean:

![](./img/do_javabean.png)

	do_resultmap:

![](./img/do_resultmap.png)

